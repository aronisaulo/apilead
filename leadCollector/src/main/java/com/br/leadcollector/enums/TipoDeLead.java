package com.br.leadcollector.enums;

public enum TipoDeLead {
    QUENTE,
    ORGANICO,
    FRIO,
}
