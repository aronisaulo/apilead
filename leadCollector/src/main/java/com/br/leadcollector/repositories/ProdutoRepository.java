package com.br.leadcollector.repositories;

import com.br.leadcollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer>{
}
