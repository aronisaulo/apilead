package com.br.leadcollector.models;
import com.br.leadcollector.enums.TipoDeLead;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name="nome_completo")
    @Size(min=8 , max=100, message = "O nome deve ter entre 8 a 100 caracteres ")
    private String nome;
    @Email(message = "formato do e-mail é invalido")
    private String email;
    private TipoDeLead tipoDeLead;

    public Lead(){}

    public Lead(String nome, String email, TipoDeLead tipoDeLead) {
        this.nome = nome;
        this.email = email;
        this.tipoDeLead = tipoDeLead;
    }

    @ManyToMany
    private List<Produto> produtos;
    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }





    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoDeLead getTipoDeLead() {
        return tipoDeLead;
    }

    public void setTipoDeLead(TipoDeLead tipoDeLead) {
        this.tipoDeLead = tipoDeLead;
    }
}
