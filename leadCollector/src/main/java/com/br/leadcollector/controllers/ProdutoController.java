package com.br.leadcollector.controllers;


import com.br.leadcollector.models.Produto;

import com.br.leadcollector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("produtos")
public class ProdutoController {
    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodosProduto(){
        return produtoService.buscarTodoProduto();
    }

    @GetMapping("/{id}")
    public Produto buscarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarPorId(id);
        if (produtoOptional.isPresent())
        {
            return produtoOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }

    }
    @PostMapping("/inserir")
    public ResponseEntity<Produto> inserirProduto(@RequestBody Produto produto) {
        Produto produtoS = produtoService.salvarProduto(produto);
       return ResponseEntity.status(201).body(produtoS);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable Integer id, @RequestBody Produto produto){
        produto.setId(id);
        Produto produtoObjeto = produtoService.atualizarProduto(produto);
        return produtoObjeto;
    }

    @DeleteMapping("/{id}")
    public Produto deletarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional =produtoService.buscarPorId(id);
        if(produtoOptional.isPresent()){
            produtoService.deletarProduto(produtoOptional.get());
            return produtoOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }
}
