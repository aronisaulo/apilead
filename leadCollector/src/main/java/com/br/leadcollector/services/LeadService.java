package com.br.leadcollector.services;

import com.br.leadcollector.models.Lead;
import com.br.leadcollector.models.Produto;
import com.br.leadcollector.repositories.LeadRepository;
import com.br.leadcollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

  //  private List<Lead> leads = new ArrayList(Arrays.asList(new Lead()));
  //private List<Lead> leads = new ArrayList(Arrays.asList(new Lead()));
    @Autowired
    private LeadRepository leadRepository;
    public Optional<Lead> buscarPorId(int id){
     //   Lead lead = leads.get(indice);
       Optional<Lead> leadOptional = leadRepository.findById(id);
       return leadOptional;
    }
    @Autowired
    private ProdutoRepository produtoRepository;
    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtoId){
        Iterable<Produto> produtoIterable  = produtoRepository.findAllById(produtoId);
        return produtoIterable;
    }

    public Lead salvarLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads(){
        Iterable<Lead> leads = leadRepository.findAll();
        return  leads;
    }

    public Lead atualizarLead(Lead lead){
        Optional<Lead> leadOptional = buscarPorId(lead.getId());
        if(leadOptional.isPresent()) {
            Lead leadData = leadOptional.get();
            if (lead.getNome()==null)
                lead.setNome(leadData.getNome());

            if(lead.getEmail() == null){
                lead.setEmail(leadData.getEmail());
            }
            if(lead.getTipoDeLead() == null){
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
        }
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void deletarLead(Lead lead){

        leadRepository.delete(lead);

    }
}
