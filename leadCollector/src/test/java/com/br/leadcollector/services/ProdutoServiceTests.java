package com.br.leadcollector.services;

import com.br.leadcollector.models.Lead;
import com.br.leadcollector.models.Produto;
import com.br.leadcollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {
    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto ;

    @BeforeEach
    public void inicializar() {
         produto = new Produto("Cafe","Cafe delicioso",19);
        //produto = new  Produto();
        produto.setId(1);

    }


    @Test
    public void testarSalvarProduto(){

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
        Produto produtoObjeto = produtoService.salvarProduto(produto);
        Assertions.assertEquals(produto,  produtoObjeto);
        Assertions.assertEquals(produto.getNome(),  produtoObjeto.getNome());

    }
    @Test
    public void testarProdutoBuscarPorID(){
        Optional<Produto> optional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(optional);
        Optional<Produto> produtoObjeto = produtoService.buscarPorId(1);
        Assertions.assertEquals(optional.get().getId(), produtoObjeto.get().getId());

    }

    @Test
    public void testarBuscarTodosProdutos(){

        Iterable<Produto> produtoIterable =  Arrays.asList(produto, produto);
        Mockito.when(produtoRepository.findAll()).thenReturn( produtoIterable);
        Iterable<Produto> produtoObjeto = produtoService.buscarTodoProduto();
        Assertions.assertEquals(produtoIterable.iterator().next().getId(), produtoObjeto.iterator().next().getId());

    }
    @Test
    public void testarAtualizarProduto(){

        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto objProduto = produtoOptional.get();
        objProduto.setDescricao("Cafe bem melhor cedo");
        Produto produtoalt = produtoService.atualizarProduto(objProduto);
        Assertions.assertEquals(produtoalt.getNome(),produto.getNome());


    }
    @Test
    public void testeDeletarProduto(){
        produtoService.deletarProduto(produto);
        Mockito.verify(produtoRepository).delete(Mockito.any(Produto.class));
    }
}
