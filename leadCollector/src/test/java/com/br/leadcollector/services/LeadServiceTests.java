package com.br.leadcollector.services;

import com.br.leadcollector.enums.TipoDeLead;
import com.br.leadcollector.models.Lead;
import com.br.leadcollector.models.Produto;
import com.br.leadcollector.repositories.LeadRepository;
import com.br.leadcollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.CleanupFailureDataAccessException;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

@SpringBootTest
public class LeadServiceTests {

  @MockBean
  LeadRepository leadRepository;

  @MockBean
  ProdutoRepository produtoRepository;

  @Autowired
  LeadService leadService;

  Lead lead;

  @MockBean
  Iterable<Lead> leadIterable ;

  @BeforeEach
  public void inicializar(){
    lead = new Lead();
    lead.setId(1);
    lead.setTipoDeLead(TipoDeLead.QUENTE);
    lead.setEmail("vinicius@gmail.com");
    lead.setNome("Vinicius");
    lead.setProdutos(Arrays.asList(new Produto()));
  }

  @Test
  public void testarSalvarLead(){

    Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
    Lead leadObjeto = leadService.salvarLead(lead);
    Assertions.assertEquals(lead, leadObjeto);
    Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());

  }

  @Test
  public void testarAtualizarLead(){

    Optional<Lead> leadOptional = Optional.of(lead);
    Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);
    Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

    Lead objLead = leadOptional.get();
    objLead.setNome("Vinicius de Moraes");

    Lead leadalt = leadService.atualizarLead(objLead);

    Assertions.assertEquals(leadalt.getNome(), lead.getNome());


  }
  @Test
  public void testarBuscarTodosLeads(){

    leadIterable =  Arrays.asList(lead, lead);
    Mockito.when(leadRepository.findAll()).thenReturn( leadIterable);
    Iterable<Lead> leadObjeto = leadService.buscarTodosLeads();
    Assertions.assertEquals(leadIterable.iterator().next().getId(), leadObjeto.iterator().next().getId());

  }

  @Test
  public void testarBuscarPorID(){
    Optional<Lead> optional = Optional.of(lead);
    Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(optional);
    Optional<Lead> leadObjeto = leadService.buscarPorId(1);
    Assertions.assertEquals(optional.get().getId(), leadObjeto.get().getId());

  }

  @Test
  public void testarBuscarTodosProdutos(){
    Produto prd1 = new Produto();
    prd1.setId(2);
    prd1.setNome("Cerveja");
    prd1.setDescricao("Sexta feira chegou é cerveja");
    prd1.setPreco(9.99);
    Iterable<Produto> produtoIterablemock = Arrays.asList(prd1);
    Mockito.when(produtoRepository.findAllById(Mockito.anyList())).thenReturn(produtoIterablemock);

    List<Integer> produtoId = new ArrayList<>();
    produtoId.add(2);
    Iterable<Produto> produtoIterable  = leadService.buscarTodosProdutos(produtoId);

    Assertions.assertEquals(produtoIterablemock.iterator().next().getId(), produtoIterable.iterator().next().getId());

  }

  @Test
  public void testeDeletarLead(){
    leadService.deletarLead(lead);
    Mockito.verify(leadRepository, Mockito.times(1)).delete(Mockito.any(Lead.class));
  }

  @Test
  public void testeDeletarLead2(){
    leadService.deletarLead(lead);
    Mockito.verify(leadRepository).delete(Mockito.any(Lead.class));
  }
}
