package com.br.leadcollector.controllers;

import com.br.leadcollector.models.Lead;
import com.br.leadcollector.models.Produto;
import com.br.leadcollector.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTests {
    Produto produto;
    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void Inicialize(){

        produto = new Produto();
        produto.setNome("Cafe");
        produto.setId(1);
        produto.setPreco(20.0);
        produto.setDescricao("Cafe muito bom");

    }


    @MockBean
    ProdutoService produtoService;


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarbuscarTodosProduto() throws Exception {

        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoService.buscarTodoProduto()).thenReturn(produtoIterable);

        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarCadastroDeProduto() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);


        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos/inserir")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    }
    @Test
    public void testarAtualizarProduto() throws Exception {
        produto.setId(2);
        produto.setNome("Cafe Pele");
        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Vivian dos Santos")));

    }


    @Test
    public void testarbuscarPorIdProduto() throws Exception {
        produto.setId(2);
        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptional);

        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(produtoOptional.get().getId())));

    }

    @Test
    public void testarDeleteProduto() throws Exception {

        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produtoOptional);
        String json = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
