package com.br.leadcollector.controllers;

import com.br.leadcollector.enums.TipoDeLead;
import com.br.leadcollector.models.Lead;
import com.br.leadcollector.models.Produto;
import com.br.leadcollector.repositories.LeadRepository;
import com.br.leadcollector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
public class LeadControllerTests {
    Lead lead;
    Produto produto;
    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void Inicialize(){
        lead = new Lead();

        lead.setNome("Saaaaulo");
        lead.setEmail("teste@teste.com.br");
        lead.setTipoDeLead(TipoDeLead.FRIO);

        produto = new Produto();
        produto.setNome("Cafe");
        produto.setId(1);
        produto.setPreco(20.0);
        produto.setDescricao("Cafe muito bom");

        lead.setProdutos(Arrays.asList(produto));
    }

    @MockBean
    LeadService leadService;


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCadastroDeLead() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtoIterable);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads/inserir")
                                           .contentType(MediaType.APPLICATION_JSON)
                                           .content(json))
                                          .andExpect(MockMvcResultMatchers.status().isCreated())
                                          .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));

    }
    @Test
    public void testarAtualizarLead() throws Exception {
        lead.setId(2);
        lead.setNome("Vivian dos Santos");
        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Vivian dos Santos")));

    }


    @Test
    public void testarbuscarPorIdLead() throws Exception {
        lead.setId(2);
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(leadOptional.get().getId())));

    }

    @Test
    public void testarbuscarTodosLead() throws Exception {

        Iterable<Lead> leadIterable = Arrays.asList(lead, lead);
        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadIterable);

        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }
    @Test
    public void testarDeleteLead() throws Exception {

        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);
        String json = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }
}
